/**
 * Finds the count of all ascends in given input of numbers.
 *
 * @example
 *  E.g. in a list [10, 12, 8, 14] there are 2 ascends - from 10 to 12 and from 8 to 14.
 *
 * In the second part it sums three consecutive numbers of that array
 * and performs the same comparison on sums.
 *
 * Note that the list is a string containing numbers separated with new line characters, therefore
 * it first needs to be split and sanitized.
 *
 * @param {string} input
 * @return {number}
 */
function ascending(input) {
    return input
        .split("\n")
        .map((value) => parseInt(value))
        .filter((value) => !isNaN(value))
        // Uncomment the mapping below for second part of the task.
        // .map((value, index, array) => value + (array[index + 1] ?? 0) + (array[index + 2] ?? 0))
        .reduce(
            (acc, current) => ({
                count: (acc.previous !== null && acc.previous < current)
                    ? (acc.count + 1)
                    : acc.count,
                previous: current,
            }),
            { count: 0, previous: null }
        )
        .count;
}

// Print the solution to standard output.
// const count = ascending("...");
// console.log(count);
